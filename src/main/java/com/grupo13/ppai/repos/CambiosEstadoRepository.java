package com.grupo13.ppai.repos;

import com.grupo13.ppai.entidades.CambioEstado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CambiosEstadoRepository extends JpaRepository<CambioEstado, Integer> {
}
