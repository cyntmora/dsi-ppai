package com.grupo13.ppai.repos;

import com.grupo13.ppai.entidades.RespuestaDeCliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaClientesRepository extends JpaRepository<RespuestaDeCliente, Integer> {
}
