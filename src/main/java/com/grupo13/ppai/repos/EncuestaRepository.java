package com.grupo13.ppai.repos;

import com.grupo13.ppai.entidades.Encuesta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EncuestaRepository extends JpaRepository<Encuesta, Integer> {
}
