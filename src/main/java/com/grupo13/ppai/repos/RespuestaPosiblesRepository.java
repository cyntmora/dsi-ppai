package com.grupo13.ppai.repos;

import com.grupo13.ppai.entidades.RespuestaPosible;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RespuestaPosiblesRepository extends JpaRepository<RespuestaPosible, Integer> {
}
