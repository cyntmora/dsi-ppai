package com.grupo13.ppai.repos;

import com.grupo13.ppai.entidades.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoRepository extends JpaRepository<Estado, Integer> {
}
