package com.grupo13.ppai.repos;

import com.grupo13.ppai.entidades.Llamada;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LlamadaRepository extends JpaRepository<Llamada, Integer> {
}
