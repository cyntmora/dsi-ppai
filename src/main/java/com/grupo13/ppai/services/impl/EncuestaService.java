package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.Encuesta;
import com.grupo13.ppai.repos.EncuestaRepository;
import com.grupo13.ppai.services.IEncuestaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EncuestaService implements IEncuestaService {
    private final EncuestaRepository encuestaRepository;

    @Autowired
    public EncuestaService(EncuestaRepository encuestaRepository) {
        this.encuestaRepository = encuestaRepository;
    }

    @Override
    public Encuesta add(Encuesta entity) {
        return null;
    }

    @Override
    public Encuesta update(Encuesta entity) {
        return null;
    }

    @Override
    public Encuesta delete(Integer integer) {
        return null;
    }

    @Override
    public Encuesta getById(Integer integer) {
        return encuestaRepository.findById(integer).get();
    }

    @Override
    public List<Encuesta> getAll() {
        return new ArrayList<>(this.encuestaRepository.findAll());
    }
}
