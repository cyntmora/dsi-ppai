package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.RespuestaDeCliente;
import com.grupo13.ppai.repos.RespuestaClientesRepository;
import com.grupo13.ppai.services.IRespuestaClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RespuestaClientesService implements IRespuestaClientesService {

    private final RespuestaClientesRepository respuestaClientesRepository;

    @Autowired
    public RespuestaClientesService(RespuestaClientesRepository respuestaClientesRepositoryç) {
        this.respuestaClientesRepository = respuestaClientesRepositoryç;
    }

    @Override
    public RespuestaDeCliente add(RespuestaDeCliente entity) {
        return null;
    }

    @Override
    public RespuestaDeCliente update(RespuestaDeCliente entity) {
        return null;
    }

    @Override
    public RespuestaDeCliente delete(Integer integer) {
        return null;
    }

    @Override
    public RespuestaDeCliente  getById(Integer integer) {
        return respuestaClientesRepository.findById(integer).get();
    }

    @Override
    public List<RespuestaDeCliente> getAll() {
        return new ArrayList<>(this.respuestaClientesRepository.findAll());
    }
}
