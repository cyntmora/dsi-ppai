package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.Encuesta;
import com.grupo13.ppai.entidades.Estado;
import com.grupo13.ppai.repos.EstadoRepository;
import com.grupo13.ppai.services.IEstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EstadoService implements IEstadoService {
    private final EstadoRepository estadoRepository;

    @Autowired
    public EstadoService(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }

    @Override
    public Estado add(Estado entity) {
        return null;
    }

    @Override
    public Estado update(Estado entity) {
        return null;
    }

    @Override
    public Estado delete(Integer integer) {
        return null;
    }

    @Override
    public Estado getById(Integer integer) {
        return estadoRepository.findById(integer).get();
    }

    @Override
    public List<Estado> getAll() {
        return new ArrayList<>(this.estadoRepository.findAll());
    }
}
