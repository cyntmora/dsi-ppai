package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.Pregunta;
import com.grupo13.ppai.repos.PreguntaRepository;
import com.grupo13.ppai.services.IPreguntaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PreguntaService implements IPreguntaService {

    private final PreguntaRepository preguntaRepository;

    @Autowired
    public PreguntaService(PreguntaRepository preguntaRepository) {
        this.preguntaRepository = preguntaRepository;
    }

    @Override
    public Pregunta add(Pregunta entity) {
        return null;
    }

    @Override
    public Pregunta update(Pregunta entity) {
        return null;
    }

    @Override
    public Pregunta delete(Integer integer) {
        return null;
    }

    @Override
    public Pregunta  getById(Integer integer) {
        return preguntaRepository.findById(integer).get();
    }

    @Override
    public List<Pregunta> getAll() {
        return new ArrayList<>(this.preguntaRepository.findAll());
    }
}
