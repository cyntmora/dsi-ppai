package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.CambioEstado;
import com.grupo13.ppai.repos.CambiosEstadoRepository;
import com.grupo13.ppai.services.ICambioEstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CambioEstadoService implements ICambioEstadoService {

    CambiosEstadoRepository cambiosEstadoRepository;

    public CambioEstadoService(CambiosEstadoRepository cambiosEstadoRepository) {
        this.cambiosEstadoRepository = cambiosEstadoRepository;
    }


    @Override
    public CambioEstado add(CambioEstado entity) {
        return null;
    }

    @Override
    public CambioEstado update(CambioEstado entity) {
        return null;
    }

    @Override
    public CambioEstado delete(Integer integer) {
        return null;
    }

    @Override
    public CambioEstado getById(Integer integer) {
        return cambiosEstadoRepository.findById(integer).get();
    }

    @Override
    public List<CambioEstado> getAll() {
        return new ArrayList<>(this.cambiosEstadoRepository.findAll());
    }
}
