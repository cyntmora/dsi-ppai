package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.Llamada;
import com.grupo13.ppai.repos.LlamadaRepository;
import com.grupo13.ppai.services.ILlamadaService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LlamadasService implements ILlamadaService {

    @Autowired
    private final LlamadaRepository llamadaRepository;

    public LlamadasService(LlamadaRepository llamadaRepository) {
        this.llamadaRepository = llamadaRepository;
    }

    @Override
    public Llamada add(Llamada entity) {
        return null;
    }

    @Override
    public Llamada update(Llamada entity) {
        return null;
    }

    @Override
    public Llamada delete(Integer integer) {
        return null;
    }

    @Override
    public Llamada  getById(Integer integer) {
        return llamadaRepository.findById(integer).get();
    }

    @Override
    @Transactional
    public List<Llamada> getAll() {
        return new ArrayList<>(this.llamadaRepository.findAll());
    }
}
