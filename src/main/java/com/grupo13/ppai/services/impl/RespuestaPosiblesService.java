package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.RespuestaPosible;
import com.grupo13.ppai.repos.RespuestaPosiblesRepository;
import com.grupo13.ppai.services.IRespuestasPosiblesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RespuestaPosiblesService implements IRespuestasPosiblesService {

    private final RespuestaPosiblesRepository respuestaPosiblesRepository;

    @Autowired
    public RespuestaPosiblesService(RespuestaPosiblesRepository respuestaPosiblesRepository) {
        this.respuestaPosiblesRepository = respuestaPosiblesRepository;
    }

    @Override
    public RespuestaPosible add(RespuestaPosible entity) {
        return null;
    }

    @Override
    public RespuestaPosible update(RespuestaPosible entity) {
        return null;
    }

    @Override
    public RespuestaPosible delete(Integer integer) {
        return null;
    }

    @Override
    public RespuestaPosible  getById(Integer integer) {
        return respuestaPosiblesRepository.findById(integer).get();
    }

    @Override
    public List<RespuestaPosible> getAll() {
        return new ArrayList<>(this.respuestaPosiblesRepository.findAll());
    }
}
