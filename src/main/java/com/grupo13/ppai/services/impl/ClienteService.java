package com.grupo13.ppai.services.impl;

import com.grupo13.ppai.entidades.Cliente;
import com.grupo13.ppai.repos.CambiosEstadoRepository;
import com.grupo13.ppai.repos.ClienteRepository;
import com.grupo13.ppai.services.IClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClienteService implements IClienteService {
    private final ClienteRepository clienteRepository;

    @Autowired
    public ClienteService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @Override
    public Cliente add(Cliente entity) {
        return null;
    }

    @Override
    public Cliente update(Cliente entity) {
        return null;
    }

    @Override
    public Cliente delete(Integer integer) {
        return null;
    }

    @Override
    public Cliente getById(Integer integer) {
        return clienteRepository.findById(integer).get();
    }

    @Override
    public List<Cliente> getAll() {
        return new ArrayList<>(this.clienteRepository.findAll());
    }
}
