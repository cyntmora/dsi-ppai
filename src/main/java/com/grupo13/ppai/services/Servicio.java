package com.grupo13.ppai.services;

import java.util.List;

public interface Servicio<T, ID>{

    T add(T entity);

    T update(T entity);

    T delete(ID id);

    T getById(ID id);

    List<T> getAll();

}
