package com.grupo13.ppai.services;

import com.grupo13.ppai.entidades.Estado;

public interface IEstadoService extends Servicio<Estado, Integer> {

}
