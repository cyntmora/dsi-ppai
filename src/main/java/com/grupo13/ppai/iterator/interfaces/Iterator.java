package com.grupo13.ppai.iterator.interfaces;

public interface Iterator {

    void primero();

    void siguiente();

    Object actual();

    boolean haTerminado();

    boolean cumpleFiltro(Object elem);
}
