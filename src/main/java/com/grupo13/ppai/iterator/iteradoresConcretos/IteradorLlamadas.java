package com.grupo13.ppai.iterator.iteradoresConcretos;


import com.grupo13.ppai.entidades.Llamada;
import com.grupo13.ppai.iterator.interfaces.Iterator;

import java.util.ArrayList;
import java.util.function.Predicate;

public class IteradorLlamadas implements Iterator {
    private Integer actual;
    private final Object[] elementos;
    private final ArrayList<Predicate<Llamada>> filtros;

    public IteradorLlamadas (Object[] elementos, ArrayList<Predicate<Llamada>> filtros){
        this.elementos = elementos;
        this.filtros = filtros;
    }

    @Override
    public void primero() {
        this.actual = 0;
    }

    @Override
    public void siguiente() {
        this.actual += 1;

    }

    @Override
    public Object actual() {
        if (cumpleFiltro(this.elementos[actual])) {
            return this.elementos[actual];
        } else return null;
    }

    @Override
    public boolean haTerminado() {
        return elementos.length == actual;
    }

    @Override
    public boolean cumpleFiltro(Object elem) {
        for (Predicate<Llamada> filtro : filtros) {
            if (!filtro.test((Llamada) elem)) return false;
        }
        return true;
    }

}
