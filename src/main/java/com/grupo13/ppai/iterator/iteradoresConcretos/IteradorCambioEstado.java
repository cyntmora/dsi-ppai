package com.grupo13.ppai.iterator.iteradoresConcretos;


import com.grupo13.ppai.entidades.CambioEstado;
import com.grupo13.ppai.entidades.Llamada;
import com.grupo13.ppai.iterator.interfaces.Iterator;

import java.util.ArrayList;
import java.util.function.Predicate;

public class IteradorCambioEstado implements Iterator {
    private Integer actual;
    private final Object[] elementos;
    private final ArrayList<Predicate<CambioEstado>> filtros;

    public IteradorCambioEstado(Object[] elementos, ArrayList<Predicate<CambioEstado>> filtros) {
        this.elementos = elementos;
        this.filtros = filtros;
    }

    @Override
    public void primero() {
        this.actual = 0;
    }

    @Override
    public void siguiente() {
        this.actual += 1;

    }

    @Override
    public Object actual() {
        return this.elementos[actual];
    }

    @Override
    public boolean haTerminado() {
        return elementos.length == actual;
    }

    @Override
    public boolean cumpleFiltro(Object elem) {
        for (Predicate<CambioEstado> filtro : filtros) {
            if (!filtro.test((CambioEstado) elem)) return false;
        }
        return true;
    }
}
