package com.grupo13.ppai;

import com.grupo13.ppai.controladores.GestorConsultarEncuesta;
import com.grupo13.ppai.interfaz.PantallaConsultarEncuestas;
import com.grupo13.ppai.services.impl.EncuestaService;
import com.grupo13.ppai.services.impl.LlamadasService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EntityScan("com.grupo13.ppai.entidades")
public class PpaiApplication {

	public static void main(String[] args) {
		PantallaConsultarEncuestas pantallaConsultarEncuestas = new PantallaConsultarEncuestas();
		ConfigurableApplicationContext applicationContext = SpringApplication.run(PpaiApplication.class, args);

		EncuestaService encuestaService = applicationContext.getBean(EncuestaService.class);
		LlamadasService llamadasService = applicationContext.getBean(LlamadasService.class);

		pantallaConsultarEncuestas.setGestorConsultarEncuesta(new GestorConsultarEncuesta(llamadasService, encuestaService));
		pantallaConsultarEncuestas.setVisible(true);
	}

}
