package com.grupo13.ppai.interfaz;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import javax.swing.plaf.metal.MetalComboBoxButton;
import javax.swing.table.DefaultTableModel;


import com.grupo13.ppai.controladores.GestorConsultarEncuesta;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import jakarta.validation.constraints.Null;


public class PantallaConsultarEncuestas extends JFrame{
	private GestorConsultarEncuesta gestorConsultarEncuesta;
	private JPanel panel1;  //usa GridBagLayout
	private JPanel panel2;  //usa GridBagLayout
	private JPanel panel3;  //usa GridBagLayout


	//elementos panel 1
	private JLabel icono;
	private JLabel texto;
	private JButton botonOpcion;
	private JButton botonCancelar;

	//elementos panel 2
	private JPanel panelFiltroInicio;
	private JPanel panelFiltroFin;
	private JLabel textoFechaInicio;
	private JLabel textoFechaFin;
	private JDateChooser entradaFechaInicio;
	private JDateChooser entradaFechaFin;
	private JButton botonCancelar2;
	private JButton botonSeleccionar;
	private JButton botonFiltrar;
	private TablaNoEditable tabla;
	private DefaultTableModel modeloTabla;
	private JScrollPane scrollTabla;


	//elementos panel 3
	private JTextArea datos;
	private JButton botonExportar;
	private JButton botonImprimir;
	private JScrollPane scrollDatos;
	private JButton botonCancelar3;


	//objetos configuracion
	private Dimension tamanioPantalla;
	private CardLayout card;	//Para usar cardLayout
	private Container contenedor;
	private String[] meses = {"1","2","3","4","5","6","7","8","9","10","11","12"};
	private String[] cabeceraLlamada = {"Cliente", "DescripcionOperador", "ObservacionAuditor"};

	//Objetos de datos
	private Date fechaInicio;
	private Date fechaFin;
	private String nombreCliente;
	private String estadoActual;
	private int duracion;
	private String[][] preguntas;
	private int llamadaSeleccionada;


	public PantallaConsultarEncuestas() {
		super();
		contenedor = getContentPane();
		tamañoPantalla();
		card = new CardLayout(); //tarjeta para navegar entre los JPanel
		contenedor.setLayout(card);

		this.setResizable(false);
		this.setSize(tamanioPantalla.width/4, tamanioPantalla.height/2);
		this.setLocationRelativeTo(this);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		iniciarPanel1();
		contenedor.add("panel1", panel1);
		iniciarPanel2();
		contenedor.add("panel2",panel2);
		iniciarPanel3();
		contenedor.add("panel3", panel3);

		card.show(contenedor, "panel1");


		this.setVisible(true);
	}

	public void setGestorConsultarEncuesta(GestorConsultarEncuesta gestor){
		this.gestorConsultarEncuesta=gestor;
	}

	private void iniciarPanel1() {
		panel1 = new JPanel();
		panel1.setBackground(new Color(53,53,53));
		panel1.setLayout(new GridBagLayout());

		GridBagConstraints cbg = new GridBagConstraints();

		iniciarLabelTexto();
		cbg.gridx = 0;
		cbg.gridy = 0;
		cbg.fill = GridBagConstraints.HORIZONTAL;
		cbg.gridwidth = GridBagConstraints.REMAINDER;
		cbg.gridheight = 1;
		cbg.weightx = 1.0;
		cbg.weighty = 1.0;
		panel1.add(texto, cbg);

		iniciarLabelIcono();
		cbg.gridx = 0;
		cbg.gridy = 1;
		cbg.fill = GridBagConstraints.NONE;
		cbg.gridwidth = GridBagConstraints.REMAINDER;
		cbg.gridheight = 1;
		cbg.weightx = 1.0;
		cbg.weighty = 1.0;
		panel1.add(icono,cbg);

		iniciarBotonOpcion();
		cbg.gridx = 0;
		cbg.gridy = 2;
		cbg.anchor = GridBagConstraints.CENTER;
		cbg.gridwidth = GridBagConstraints.REMAINDER;
		cbg.gridheight = 1;
		cbg.weightx = 1.0;
		cbg.weighty = 1.0;
		panel1.add(botonOpcion, cbg);

		iniciarBotonCancelar();
		cbg.gridx = 0;
		cbg.gridy = 3;
		cbg.anchor = GridBagConstraints.NORTH;
		cbg.gridwidth = GridBagConstraints.REMAINDER;
		cbg.gridheight = 1;
		cbg.weightx = 1.0;
		cbg.weighty = 1.0;
		panel1.add(botonCancelar,cbg);

	}

	private void iniciarPanel2() {
		panel2 = new JPanel();
		panel2.setBackground(new Color(53,53,53));
		panel2.setLayout(new GridBagLayout());

		GridBagConstraints cbg = new GridBagConstraints();

		iniciarPanelFiltroInicio();
		cbg.gridx = 0;
		cbg.gridy = 0;
		cbg.weighty=1.0;
		cbg.weightx=1.0;
		cbg.insets = new Insets(30,0,0,0);
		cbg.anchor = GridBagConstraints.NORTHEAST;
		panel2.add(panelFiltroInicio,cbg);

		iniciarPanelFitroFin();
		cbg.gridx = 0;
		cbg.gridy = 1;
		cbg.insets = new Insets(-50,0,0,0);
		cbg.anchor = GridBagConstraints.NORTHEAST;
		panel2.add(panelFiltroFin,cbg);


		iniciarBotonFiltrar();
		cbg.gridx = 1;
		cbg.gridy = 0;
		cbg.insets = new Insets(0,15,0,0);
		cbg.anchor = GridBagConstraints.WEST;
		panel2.add(botonFiltrar,cbg);

		String [][] nulo = {{" "," ","",""}};
		iniciarTabla(nulo);
		cbg.gridx = 0;
		cbg.gridy = 2;
		cbg.insets = new Insets(0,0,0,0);
		cbg.anchor = GridBagConstraints.NORTH;
		cbg.gridwidth = GridBagConstraints.REMAINDER;
		panel2.add(scrollTabla,cbg);

		iniciarBotonSeleccionar();
		cbg.gridx = 1;
		cbg.gridy = 3;
		cbg.gridwidth = 1;
		panel2.add(botonSeleccionar,cbg);

		iniciarBotonCancelar2();
		cbg.gridx = 0;
		cbg.gridy = 3;
		panel2.add(botonCancelar2,cbg);

	}



	private void iniciarPanel3() {
		panel3 = new JPanel();
		panel3.setBackground(new Color(53,53,53));
		panel3.setLayout(new GridBagLayout());

		GridBagConstraints cbg = new GridBagConstraints();

		iniciarDatos();
		cbg.gridx = 0;
		cbg.gridy = 0;
		cbg.gridwidth=3;
		cbg.weighty=1.0;
		cbg.weightx=1.0;
		cbg.anchor = GridBagConstraints.CENTER;
		cbg.fill = GridBagConstraints.REMAINDER;
		panel3.add(scrollDatos, cbg);

		iniciarBotonCancelar3();
		cbg.gridx = 0;
		cbg.gridy = 1;
		cbg.gridwidth=1;
		cbg.insets = new Insets(0,0,0,0);
		cbg.fill = GridBagConstraints.NONE;
		cbg.anchor = GridBagConstraints.NORTHEAST;
		panel3.add(botonCancelar, cbg);

		iniciarBotonImprimir();
		cbg.gridx = 1;
		cbg.gridy = 1;
		cbg.insets = new Insets(0,0,10,0);
		cbg.fill = GridBagConstraints.NONE;
		cbg.anchor = GridBagConstraints.NORTHEAST;
		panel3.add(botonImprimir, cbg);

		iniciarBotonExportar();
		cbg.gridx = 2;
		cbg.gridy = 1;
		cbg.insets = new Insets(0,10,10,0);
		cbg.fill = GridBagConstraints.NONE;
		cbg.anchor = GridBagConstraints.NORTHWEST;
		panel3.add(botonExportar, cbg);

	}

	private void iniciarPanelFiltroInicio() {
		this.panelFiltroInicio = new JPanel();
		this.panelFiltroInicio.setBackground(new Color(53,53,53));
		this.panelFiltroInicio.setLayout(new BoxLayout(panelFiltroInicio, BoxLayout.X_AXIS));

		iniciarTextoFechaInicio();
		panelFiltroInicio.add(textoFechaInicio);

		entradaFechaInicio = new JDateChooser();
		entradaFechaInicio.setDateFormatString("dd/MM/yyyy");
		panelFiltroInicio.add(entradaFechaInicio);

	}

	private void iniciarPanelFitroFin() {
		this.panelFiltroFin = new JPanel();
		this.panelFiltroFin.setBackground(new Color(53,53,53));
		this.panelFiltroFin.setLayout(new BoxLayout(panelFiltroFin, BoxLayout.X_AXIS));

		iniciarTextoFechaFin();
		panelFiltroFin.add(textoFechaFin);

		entradaFechaFin = new JDateChooser();
		entradaFechaFin.setDateFormatString("dd/MM/yyyy");

		Date fechaActual = new Date();
		entradaFechaFin.setDate(fechaActual);
		tomarFechaFin();

		panelFiltroFin.add(entradaFechaFin);
	}




	private void iniciarBotonExportar() {
		botonExportar = new JButton("Exportar");
		botonExportar.setBorderPainted(false);
		botonExportar.setBackground(new Color(40,75,99));
		botonExportar.setFocusable(false);
		botonExportar.setFont(new Font("Roboto",1, 12));
		botonExportar.setForeground(new Color(255,255,255));
		botonExportar.setEnabled(false);
		botonExportar.addActionListener(arg0 -> tomarOpcionCsv());
	}

	private void iniciarBotonImprimir() {
		botonImprimir = new JButton("Imprimir");
		botonImprimir.setBorderPainted(false);
		botonImprimir.setBackground(new Color(40,75,99));
		botonImprimir.setFocusable(false);
		botonImprimir.setFont(new Font("Roboto",1, 12));
		botonImprimir.setForeground(new Color(255,255,255));
		botonImprimir.setEnabled(false);
	}

	private void iniciarBotonOpcion() {
		botonOpcion = new JButton("CONSULTAR ENCUESTAS");
		botonOpcion.setBorderPainted(false);
		botonOpcion.setBackground(new Color(40,75,99));
		botonOpcion.setFocusable(false);
		botonOpcion.setFont(new Font("Roboto",1, 12));
		botonOpcion.setForeground(new Color(255,255,255));
		botonOpcion.addActionListener(arg0 -> habilitarPantalla());
	}

	private void iniciarBotonCancelar() {
		botonCancelar = new JButton("cancelar");
		botonCancelar.setBorderPainted(false);
		botonCancelar.setBackground(new Color(40,75,99));
		botonCancelar.setFocusable(false);
		botonCancelar.setFont(new Font("Roboto",1, 12));
		botonCancelar.setForeground(new Color(255,255,255));
		botonCancelar.addActionListener(arg0 -> System.exit(0));
	}

	private void iniciarBotonFiltrar() {
		botonFiltrar = new JButton("Filtrar");
		botonFiltrar.setBorderPainted(false);
		botonFiltrar.setBackground(new Color(40,75,99));
		botonFiltrar.setFocusable(false);
		botonFiltrar.setFont(new Font("Roboto",1, 12));
		botonFiltrar.setForeground(new Color(255,255,255));
		botonFiltrar.addActionListener(arg0 -> {
            tomarFechaInicio();
            tomarFechaFin();
			if (fechaInicio.after(fechaFin)){
				JOptionPane.showMessageDialog(panel2, "Periodo mal ingresado", "Fechas", JOptionPane.INFORMATION_MESSAGE);

			}else{
				gestorConsultarEncuesta.tomarFechaInicioFin(fechaInicio, fechaFin, PantallaConsultarEncuestas.this);
			}

        });
	}

	private void iniciarBotonSeleccionar() {
		botonSeleccionar = new JButton("seleccionar");
		botonSeleccionar.setBorderPainted(false);
		botonSeleccionar.setBackground(new Color(40,75,99));
		botonSeleccionar.setFocusable(false);
		botonSeleccionar.setFont(new Font("Roboto",1, 12));
		botonSeleccionar.setForeground(new Color(255,255,255));
		botonSeleccionar.setEnabled(false);
		botonSeleccionar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				llamadaSeleccionada = tomarSeleccionLlamada();
				gestorConsultarEncuesta.tomarSeleccionLlamada(llamadaSeleccionada, PantallaConsultarEncuestas.this);
			}
		});
	}

	private void iniciarBotonCancelar2() {
		botonCancelar2 = new JButton("cancelar");
		botonCancelar2.setBorderPainted(false);
		botonCancelar2.setBackground(new Color(40,75,99));
		botonCancelar2.setFocusable(false);
		botonCancelar2.setFont(new Font("Roboto",1, 12));
		botonCancelar2.setForeground(new Color(255,255,255));
		botonCancelar2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
	}

	private void iniciarBotonCancelar3() {
		botonCancelar2 = new JButton("cancelar");
		botonCancelar2.setBorderPainted(false);
		botonCancelar2.setBackground(new Color(40,75,99));
		botonCancelar2.setFocusable(false);
		botonCancelar2.setFont(new Font("Roboto",1, 12));
		botonCancelar2.setForeground(new Color(255,255,255));
		botonCancelar2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				card.show(contenedor,"panel2");
			}
		});
	}

	private void iniciarTabla(String[][] datos) {
		modeloTabla = new DefaultTableModel(cabeceraLlamada, 0);
		tabla = new TablaNoEditable(modeloTabla) ;
		tabla.setBackground(new Color(217,217,217));
		tabla.setFont(new Font("Roboto", 5, 12));
		tabla.setFillsViewportHeight(true);
		tabla.setFocusable(false);
		tabla.setDragEnabled(false);
		scrollTabla = new JScrollPane(tabla);
	}

	private void iniciarLabelIcono() {
		icono = new JLabel();
		icono.setBounds(100, 100, 150, 150);
		asociarImagenALabel(icono,"./encuesta2.png");

	}

	private void iniciarDatos() {
		datos = new JTextArea();
		datos.setBackground((new Color(217,217,217)));
		datos.setForeground(Color.BLACK);
		datos.setEditable(false);
		datos.setFont(new Font("Roboto",4,12));

		scrollDatos = new JScrollPane(datos);
		scrollDatos.setBorder(null);
	}

	private void iniciarLabelTexto() {
		texto = new JLabel("ENCUESTAS DE LLAMADAS");
		texto.setOpaque(true);
		texto.setBackground(new Color(40,75,99));
		texto.setForeground(new Color(255,255,255));
		texto.setFont(new Font("Roboto", 1, 23));
		texto.setHorizontalAlignment(JLabel.CENTER);


		texto.repaint();
		texto.setVisible(true);
	}

	private void iniciarTextoFechaInicio() {
		textoFechaInicio = new JLabel("Fecha:");
		textoFechaInicio.setFont(new Font("Roboto", 1, 12));
		textoFechaInicio.setOpaque(true);
		textoFechaInicio.setBackground(new Color(53,53,53));
		textoFechaInicio.setForeground(Color.WHITE);
	}

	private void iniciarTextoFechaFin() {
		textoFechaFin = new JLabel("Fecha:");
		textoFechaFin.setFont(new Font("Roboto", 1, 12));
		textoFechaFin.setOpaque(true);
		textoFechaFin.setBackground(new Color(53,53,53));
		textoFechaFin.setForeground(Color.WHITE);
	}

	private void asociarImagenALabel(JLabel label, String ruta) {
		ImageIcon imagen = new ImageIcon(ruta);
		Icon icono = new ImageIcon(imagen.getImage().getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_DEFAULT));
		label.setIcon(icono);
		label.repaint();
	}

	private void tamañoPantalla() {
		Toolkit datosPantalla = Toolkit.getDefaultToolkit();
		tamanioPantalla = datosPantalla.getScreenSize();
	}

	private void habilitarPantalla() {
		this.setResizable(true);
		this.setSize(tamanioPantalla.width, tamanioPantalla.height);
		this.setLocationRelativeTo(null);
		card.show(contenedor, "panel2");

		gestorConsultarEncuesta.getLlamadasPorPeriodo(this);
	}


	/*private void exportarInforme() {
		//Creo libro excel
		Workbook libro = new XSSFWorkbook();

		//Creo hoja
		org.apache.poi.ss.usermodel.Sheet hoja = libro.createSheet();

		//seteo filas de cabeceras y datos excel
		int cont = 0;
		int saltar = -2;
		for (int i=1; i<cabeceraDatosLlamada.length+1; i++) {
			if (i > 4 && saltar == 2){
					cont += 1;
					saltar = 0;
			}
				Row fila = hoja.createRow(i+cont);
				Cell celda = fila.createCell(1);
				celda.setCellValue(cabeceraDatosLlamada[i-1]);
				celda = fila.createCell(2);
				celda.setCellValue(datosLlamada[i-1]);
				saltar += 1;

		}

		//Exporto el archivo
		try {
			OutputStream exportar = new FileOutputStream("informe.xlsx");
			libro.write(exportar);
			exportar.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}*/

	public void pedirFechaInicioFechaFin(){
		entradaFechaInicio.setEnabled(true);
		entradaFechaFin.setEnabled(true);
	}

	private void tomarFechaInicio() {
		this.fechaInicio = entradaFechaInicio.getDate();
	}

	private void tomarOpcionCsv() {
		gestorConsultarEncuesta.tomarOpcionCsv(this, this.nombreCliente, this.estadoActual, this.duracion, this.preguntas);
	}

	private void tomarFechaFin() {
		this.fechaFin = entradaFechaFin.getDate();

	}

	public void mostrarLlamadasConEncuestaRespondida(String[][] llamadas){
		limpiarTabla();
		tabla.setVisible(false);
		scrollTabla.setVisible(false);
		if (llamadas.length == 0 || llamadas == null){
			JOptionPane.showMessageDialog(panel2,"No hay llamadas para el periodo seleccionado", "Llamadas", JOptionPane.INFORMATION_MESSAGE);
		}
		for (String[] llamada : llamadas) {
			modeloTabla.addRow(llamada);
		}
		tabla.setVisible(true);

		scrollTabla.setVisible(true);
		panel2.repaint();

	}

	private void limpiarTabla(){
		int filas = tabla.getRowCount();
		int cont = 0;
		if (filas != 0) {
			while (filas != cont) {
				modeloTabla.removeRow(0);
				cont += 1;
			}
		}
	}

	public void pedirSeleccionLlamada(){
		botonSeleccionar.setEnabled(true);
	}

	public int tomarSeleccionLlamada(){
		int index = tabla.getSelectedRow();
		if (index == -1){
			JOptionPane.showMessageDialog(panel2, "Seleccione una llamada", "LLamada no Seleccionada", JOptionPane.INFORMATION_MESSAGE);
		}
		return index;
	}

	public void mostrarDatosLlamadaSeleccionada(String nombreCliente, String estadoActual, int duracion, String[][] preguntas ) {
		this.nombreCliente = nombreCliente;
		this.estadoActual = estadoActual;
		this.duracion = duracion;
		this.preguntas = preguntas;

		datos.setText("Nombre cliente: ");

		datos.append(this.nombreCliente + "\n\n");

		datos.append("Estado actual");

		datos.append(this.estadoActual + "\n\n");

		datos.append("Duracion: ");

		datos.append(this.duracion + "\n\n");

		datos.append("Descripcion encuesta: ");

		datos.append(this.preguntas[0][2] + "\n\n\n");

		for (String[] pregunta : this.preguntas) {
			datos.append("Pregunta: ");
			datos.append(pregunta[0] + "\n");
			datos.append("Respuesta: ");
			datos.append(pregunta[1] + "\n\n");
		}

		card.show(contenedor, "panel3");
	}

	public void mostrarOpcionesVisualizacion(){
		botonExportar.setEnabled(true);
		botonImprimir.setEnabled(true);


	}

	public void notificarGeneracionArchivo(String path){
		JOptionPane.showMessageDialog (panel3, "Archivo generado. ruta:" + path);
	}
}


class TablaNoEditable extends JTable {
	public TablaNoEditable(DefaultTableModel modelo) {
		super(modelo);
	}

	public boolean isCellEditable(int fila, int columna) {
		return false;
	}
}
