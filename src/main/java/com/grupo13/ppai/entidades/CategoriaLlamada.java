package com.grupo13.ppai.entidades;

import java.util.ArrayList;

public class CategoriaLlamada {
    private String audioMensajesOpciones;
    private String mensajesOpciones;
    private String nombre;
    private int nroOrden;
    private ArrayList<OpcionLlamada> opcion;

    public CategoriaLlamada(String audioMensajesOpciones, String mensajesOpciones, String nombre, int nroOrden, ArrayList<OpcionLlamada> opcion) {
        this.audioMensajesOpciones = audioMensajesOpciones;
        this.mensajesOpciones = mensajesOpciones;
        this.nombre = nombre;
        this.nroOrden = nroOrden;
        this.opcion = opcion;
    }

    public String getAudioMensajesOpciones() {
        return audioMensajesOpciones;
    }

    public void setAudioMensajesOpciones(String audioMensajesOpciones) {
        this.audioMensajesOpciones = audioMensajesOpciones;
    }

    public String getMensajesOpciones() {
        return mensajesOpciones;
    }

    public void setMensajesOpciones(String mensajesOpciones) {
        this.mensajesOpciones = mensajesOpciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNroOrden() {
        return nroOrden;
    }

    public void setNroOrden(int nroOrden) {
        this.nroOrden = nroOrden;
    }

    public ArrayList<OpcionLlamada> getOpcion() {
        return opcion;
    }

    public void setOpcion(ArrayList<OpcionLlamada> opcion) {
        this.opcion = opcion;
    }
}
