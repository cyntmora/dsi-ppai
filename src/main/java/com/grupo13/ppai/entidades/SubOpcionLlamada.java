package com.grupo13.ppai.entidades;

public class SubOpcionLlamada {
    private String orden;
    private int nroOrden;
    private Validacion validacionRequerida;

    public SubOpcionLlamada(String orden, int nroOrden, Validacion validacionRequerida) {
        this.orden = orden;
        this.nroOrden = nroOrden;
        this.validacionRequerida = validacionRequerida;
    }

    public SubOpcionLlamada() {
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public int getNroOrden() {
        return nroOrden;
    }

    public void setNroOrden(int nroOrden) {
        this.nroOrden = nroOrden;
    }

    public Validacion getValidacionRequerida() {
        return validacionRequerida;
    }

    public void setValidacionRequerida(Validacion validacionRequerida) {
        this.validacionRequerida = validacionRequerida;
    }

    public boolean esNro(int nro){
        return nro == this.nroOrden;
    }
}
