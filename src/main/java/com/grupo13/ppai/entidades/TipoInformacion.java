package com.grupo13.ppai.entidades;

public class TipoInformacion {
    private String descripcion;

    public TipoInformacion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
