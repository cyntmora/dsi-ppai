package com.grupo13.ppai.entidades;

import java.util.Date;

public class Usuario {
    private boolean activo;
    private Date fechaAlta;
    private String nombreUsuario;
    private String password;
    private Perfil perfil;

    public Usuario(boolean activo, Date fechaAlta, String nombreUsuario, String password, Perfil perfil) {
        this.activo = activo;
        this.fechaAlta = fechaAlta;
        this.nombreUsuario = nombreUsuario;
        this.password = password;
        this.perfil = perfil;
    }

    public Usuario() {
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public Date getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Date fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
}
