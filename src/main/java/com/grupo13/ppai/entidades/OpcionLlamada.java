package com.grupo13.ppai.entidades;

import java.util.ArrayList;

public class OpcionLlamada {
    private String audioMensajesSubopciones;
    private String mensajeSubopciones;
    private String nombre;
    private int nroOrden;
    private ArrayList<Validacion> validacionesRequeridas;
    private ArrayList<SubOpcionLlamada> subOpcionLlamada;

    public OpcionLlamada(String audioMensajesSubopciones, String mensajeSubopciones, String nombre, int nroOrden, ArrayList<Validacion> validacionesRequeridas, ArrayList<SubOpcionLlamada> subOpcionLlamada) {
        this.audioMensajesSubopciones = audioMensajesSubopciones;
        this.mensajeSubopciones = mensajeSubopciones;
        this.nombre = nombre;
        this.nroOrden = nroOrden;
        this.validacionesRequeridas = validacionesRequeridas;
        this.subOpcionLlamada = subOpcionLlamada;
    }

    public OpcionLlamada() {
    }

    public String getAudioMensajesSubopciones() {
        return audioMensajesSubopciones;
    }

    public void setAudioMensajesSubopciones(String audioMensajesSubopciones) {
        this.audioMensajesSubopciones = audioMensajesSubopciones;
    }

    public String getMensajeSubopciones() {
        return mensajeSubopciones;
    }

    public void setMensajeSubopciones(String mensajeSubopciones) {
        this.mensajeSubopciones = mensajeSubopciones;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNroOrden() {
        return nroOrden;
    }

    public void setNroOrden(int nroOrden) {
        this.nroOrden = nroOrden;
    }

    public ArrayList<Validacion> getValidacionesRequeridas() {
        return validacionesRequeridas;
    }

    public void setValidacionesRequeridas(ArrayList<Validacion> validacionesRequeridas) {
        this.validacionesRequeridas = validacionesRequeridas;
    }

    public ArrayList<SubOpcionLlamada> getSubOpcionLlamada() {
        return subOpcionLlamada;
    }

    public void setSubOpcionLlamada(ArrayList<SubOpcionLlamada> subOpcionLlamada) {
        this.subOpcionLlamada = subOpcionLlamada;
    }

    private String getDescripcionConSubOpcion(){
        // TODO
        return "";
    }

}
