package com.grupo13.ppai.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "encuestas")
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
public class Encuesta {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String descripcion;
    @Column
    private String fechaFinVigencia;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "encuestaXPregunta",
            joinColumns = @JoinColumn(name = "idEncuesta"),
            inverseJoinColumns = @JoinColumn(name = "idPregunta")
    )
    @JsonIgnore
    private List<Pregunta> preguntas;

    public String getDescripcionEncuesta() {
        return descripcion;
    }

    public boolean esEncuestaDeCliente(Cliente cliente){
        // TODO
        return true;
    }
}
