package com.grupo13.ppai.entidades;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "estados")
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
public class Estado {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String nombre;

    public Estado(String nombre) {
        this.nombre = nombre;
    }

    public boolean esFinalizada(){
        return nombre.equals("FINALIZADA");
    }

    public boolean esIniciada(){
        return nombre.equals("INICIADA");
    }

}
