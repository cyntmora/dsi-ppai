package com.grupo13.ppai.entidades;

import com.grupo13.ppai.iterator.interfaces.IAgregado;
import com.grupo13.ppai.iterator.interfaces.Iterator;
import com.grupo13.ppai.iterator.iteradoresConcretos.IteradorCambioEstado;
import com.grupo13.ppai.iterator.iteradoresConcretos.IteradorLlamadas;
import com.zaxxer.hikari.util.SuspendResumeLock;
import jakarta.persistence.*;
import lombok.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "llamadas")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Llamada implements IAgregado {

    @Id
    @Column(name = "id")
    private Integer idLlamada;

    @Column
    private String descripcionOperador;

    @Column(name = "detalleAccionRequerida")
    private String detalleAccionRequerida;

    @Column(name = "duracion")
    private int duracion;

    @Column(name = "encuestaEnviada")
    private boolean encuestaEnviada;

    @Column(name = "observacionAuditor")
    private String observacionAuditor;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "idLlamada")
    private List<RespuestaDeCliente> respuestasDeEncuesta;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "idLlamada")
    private List<CambioEstado> cambioEstados;


    @OneToOne
    @JoinColumn(name = "idCliente", referencedColumnName = "id")
    private Cliente cliente;


    private int calcularDuracion(){
        // TODO
        return 0;
    }

    public String getNombreClienteDeLlamada(){
        return cliente.getNombreCompleto();
    }

    public String getEstadoActual(){
        CambioEstado actual = null;
        for (CambioEstado cambioEstado: cambioEstados){
            if (cambioEstado.esUltimoEstado(cambioEstados.get(cambioEstados.size()-1))){
                actual = cambioEstado;
                break;
            }
        }
        return actual.getNombreEstado();

    }

    public boolean esDePeriodo(Date fechaInicio, Date fechaFin) {
        Iterator iterator = crearIterator(this.cambioEstados.toArray());
        iterator.primero();
        Date firstDate = null;
        while (!iterator.haTerminado()) {
            CambioEstado actual = (CambioEstado) iterator.actual();
            if (actual!=null) {
                Date startDate = actual.getFechaHoraInicio();
                if (firstDate==null || firstDate.after(startDate)) {
                    firstDate = startDate;
                }
            }
            iterator.siguiente();
        }
        if (firstDate!=null) {
            boolean esAnteriorAFechaFin = firstDate.before(fechaFin);
            boolean esPosteriorFechaInicio = firstDate.after(fechaInicio);
            return (esPosteriorFechaInicio && esAnteriorAFechaFin);
        }

        return false;
    }

    public boolean tieneRespuesta(){
        return !this.respuestasDeEncuesta.isEmpty();
    }

    @Override
    public Iterator crearIterator(Object[] elementos) {
        return new IteradorCambioEstado(elementos, new ArrayList<>());
    }
}
