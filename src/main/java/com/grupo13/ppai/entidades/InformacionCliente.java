package com.grupo13.ppai.entidades;

public class InformacionCliente {
    private String datoAValidar;
    private Validacion validacion;
    private OpcionValidacion opcionCorrecta;
    private TipoInformacion tipoInfomacion;

    public InformacionCliente(String datoAValidar, Validacion validacion, OpcionValidacion opcionCorrecta, TipoInformacion tipoInfomacion) {
        this.datoAValidar = datoAValidar;
        this.validacion = validacion;
        this.opcionCorrecta = opcionCorrecta;
        this.tipoInfomacion = tipoInfomacion;
    }

    public String getDatoAValidar() {
        return datoAValidar;
    }

    public void setDatoAValidar(String datoAValidar) {
        this.datoAValidar = datoAValidar;
    }

    public Validacion getValidacion() {
        return validacion;
    }

    public void setValidacion(Validacion validacion) {
        this.validacion = validacion;
    }

    public OpcionValidacion getOpcionCorrecta() {
        return opcionCorrecta;
    }

    public void setOpcionCorrecta(OpcionValidacion opcionCorrecta) {
        this.opcionCorrecta = opcionCorrecta;
    }

    public TipoInformacion getTipoInfomacion() {
        return tipoInfomacion;
    }

    public void setTipoInfomacion(TipoInformacion tipoInfomacion) {
        this.tipoInfomacion = tipoInfomacion;
    }

    public boolean esValidacion(Validacion validacion){
        return this.validacion.equals(validacion);
    }

    public boolean esInformacionCorrecta(){
        // TODO
        return false;
    }
}
