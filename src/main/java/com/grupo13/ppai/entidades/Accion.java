package com.grupo13.ppai.entidades;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Accion {
    private String descripcion;

    public Accion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Accion() {
    }

}
