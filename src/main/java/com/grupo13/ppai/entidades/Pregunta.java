package com.grupo13.ppai.entidades;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "preguntas")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Pregunta {

    @Id
    @Column(name = "id")
    private Integer idPregunta;

    @Column(name = "pregunta")
    private String pregunta;

    /*@ManyToMany
    @JsonManagedReference
    @JoinTable(
            name = "preguntaXRespuestaPosible",
            joinColumns = @JoinColumn(name = "idRespuestaPosible"),
            inverseJoinColumns = @JoinColumn(name = "idPreguntaPosible")
    )*/
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "preguntaXRespuestaPosible",
            joinColumns = @JoinColumn(name = "idRespuestaPosible"),
            inverseJoinColumns = @JoinColumn(name = "idPreguntaPosible")
    )
    @JsonIgnore
    private List<RespuestaPosible> respuestas;


    public String getDescripcion() {
        return pregunta;
    }

    public List<RespuestaPosible> listarRespuestasPosibles() {
        return respuestas;
    }

    public String getDescripcionEncuesta(ArrayList<Encuesta> encuestas){
        for (Encuesta encuesta: encuestas){
            if (encuesta.getPreguntas().contains(this)){
                return encuesta.getDescripcionEncuesta();
            }
        }
        return "no se encontró";
    }
}
