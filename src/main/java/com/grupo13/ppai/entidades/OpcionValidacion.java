package com.grupo13.ppai.entidades;

public class OpcionValidacion {
    private boolean correcta;
    private String descripcion;

    public OpcionValidacion(boolean correcta, String descripcion) {
        this.correcta = correcta;
        this.descripcion = descripcion;
    }

    public boolean esCorrecta() {
        return correcta;
    }

    public void setCorrecta(boolean correcta) {
        this.correcta = correcta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
