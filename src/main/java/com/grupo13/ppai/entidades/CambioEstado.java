package com.grupo13.ppai.entidades;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter
@Entity
@Table(name = "cambiosEstados")
@AllArgsConstructor @NoArgsConstructor
public class CambioEstado {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private Date fechaHoraInicio;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "idEstado")
    private Estado estado;


    public boolean esUltimoEstado(CambioEstado cambioEstado){
        return this.equals(cambioEstado);
    }


    public String getNombreEstado(){
        return this.estado.getNombre();
    }

}
