package com.grupo13.ppai.entidades;

import java.util.ArrayList;

public class Validacion {
    private String audioMensajeValidacion;
    private String nombre;
    private ArrayList<OpcionValidacion> opcionesValidacion;
    private TipoInformacion tipo;

    public Validacion(String audioMensajeValidacion, String nombre, ArrayList<OpcionValidacion> opcionesValidacion, TipoInformacion tipo) {
        this.audioMensajeValidacion = audioMensajeValidacion;
        this.nombre = nombre;
        this.opcionesValidacion = opcionesValidacion;
        this.tipo = tipo;
    }

    public String getAudioMensajeValidacion() {
        return audioMensajeValidacion;
    }

    public void setAudioMensajeValidacion(String audioMensajeValidacion) {
        this.audioMensajeValidacion = audioMensajeValidacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<OpcionValidacion> getOpcionesValidacion() {
        return opcionesValidacion;
    }

    public void setOpcionesValidacion(ArrayList<OpcionValidacion> opcionesValidacion) {
        this.opcionesValidacion = opcionesValidacion;
    }

    public TipoInformacion getTipo() {
        return tipo;
    }

    public void setTipo(TipoInformacion tipo) {
        this.tipo = tipo;
    }

    public String getMensajeValidacion(){
        // TODO
        return "";
    }
}

