package com.grupo13.ppai.entidades;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;

@Entity
@Table(name = "respuestasClientes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaDeCliente {

    @Id
    @Column(name = "id")
    private Integer idRespuestaCliente;

    @Column(name = "fechaEncuesta")
    private String fechaEncuesta;

    @OneToOne
    @JoinColumn(name = "idRespuestaPosible", referencedColumnName = "id")
    private RespuestaPosible respuestaSeleccionada;

    public String getDescripcionPregunta(ArrayList<Encuesta> encuestas){
        return this.respuestaSeleccionada.getDescripcionPregunta(encuestas);
    }

    public String getDescripcionEncuesta(ArrayList<Encuesta> encuestas){
        return this.respuestaSeleccionada.getDescripcionEncuesta(encuestas);
    }

    public String getDescripcionRespuesta(){
        return this.respuestaSeleccionada.getDescripcion();
    }


}
