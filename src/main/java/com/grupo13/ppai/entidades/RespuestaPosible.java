package com.grupo13.ppai.entidades;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "respuestasPosibles")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RespuestaPosible {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "valor")
    private Integer valor;

    /*@ManyToMany(mappedBy = "preguntas")
    @JsonBackReference
    List<Pregunta> preguntasLista;*/
    public String getDescripcionPregunta(ArrayList<Encuesta> encuestas){
        for (Encuesta encuesta: encuestas){
            for (Pregunta pregunta: encuesta.getPreguntas()){
                if (pregunta.listarRespuestasPosibles().contains(this)){
                    return pregunta.getDescripcion();
                }
            }
        }
        return "no se encontró";
    }

    public String getDescripcionEncuesta(ArrayList<Encuesta> encuestas){
        for (Encuesta encuesta: encuestas){
            for (Pregunta pregunta: encuesta.getPreguntas()){
                if (pregunta.listarRespuestasPosibles().contains(this)){
                    return encuesta.getDescripcionEncuesta();
                }
            }
        }
        return "no se encontró";
    }
}
