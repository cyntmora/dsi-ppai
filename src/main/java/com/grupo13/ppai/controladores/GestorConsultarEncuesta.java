package com.grupo13.ppai.controladores;

import com.grupo13.ppai.entidades.Encuesta;
import com.grupo13.ppai.entidades.Llamada;
import com.grupo13.ppai.entidades.RespuestaDeCliente;
import com.grupo13.ppai.interfaz.PantallaConsultarEncuestas;
import com.grupo13.ppai.iterator.interfaces.IAgregado;
import com.grupo13.ppai.iterator.interfaces.Iterator;
import com.grupo13.ppai.iterator.iteradoresConcretos.IteradorLlamadas;
import com.grupo13.ppai.services.impl.EncuestaService;
import com.grupo13.ppai.services.impl.LlamadasService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Predicate;

public class GestorConsultarEncuesta implements IAgregado {

    private Date fechaInicio;
    private Llamada seleccionada;
    private final ArrayList<Llamada> todasLasLlamadas;
    private ArrayList<Llamada> llamadasFiltradas;
    private final ArrayList<Encuesta> todasLasEncuestas;
    private Date fechaFin;

    public GestorConsultarEncuesta(LlamadasService llamadasService, EncuestaService encuestaService) {
        this.todasLasEncuestas = (ArrayList<Encuesta>) encuestaService.getAll();
        this.todasLasLlamadas = (ArrayList<Llamada>) llamadasService.getAll();
    }

    public void getLlamadasPorPeriodo(PantallaConsultarEncuestas pantallaConsultarEncuestas){
        pantallaConsultarEncuestas.pedirFechaInicioFechaFin();
    }

    public void tomarFechaInicioFin(Date fechaInicio, Date fechaFin, PantallaConsultarEncuestas pantallaConsultarEncuestas){
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        if (this.verificarPeriodoValido()) {
             this.llamadasFiltradas = buscarLlamadasConEncuestaRespondida();
            ArrayList<String[]> resultLlamadas= new ArrayList<>();
            for (Llamada llamada: llamadasFiltradas){
                resultLlamadas.add(new String[]{llamada.getCliente().getNombreCompleto(), llamada.getDescripcionOperador(), llamada.getObservacionAuditor()});
            }
            pantallaConsultarEncuestas.mostrarLlamadasConEncuestaRespondida(resultLlamadas.toArray(new String[resultLlamadas.size()][]));
            pantallaConsultarEncuestas.pedirSeleccionLlamada();
        }
    }

    public void tomarSeleccionLlamada(int index, PantallaConsultarEncuestas pantallaConsultarEncuestas){
        this.seleccionada = this.llamadasFiltradas.get(index);
        String nombreCliente = this.seleccionada.getNombreClienteDeLlamada();
        String estadoActual = this.seleccionada.getEstadoActual();
        int duracion = this.seleccionada.getDuracion();
        ArrayList<String[]> result = getDatosRespuestas();
        pantallaConsultarEncuestas.mostrarDatosLlamadaSeleccionada(nombreCliente, estadoActual, duracion, result.toArray(new String[result.size()][]));
        pantallaConsultarEncuestas.mostrarOpcionesVisualizacion();
    }

    public boolean verificarPeriodoValido(){
        return fechaInicio.before(fechaFin);
    }

    public ArrayList<Llamada> buscarLlamadasConEncuestaRespondida() {
        ArrayList<Llamada> llamadas = new ArrayList<>();
        Iterator iterator = crearIterator(todasLasLlamadas.toArray());
        iterator.primero();

        while (!iterator.haTerminado()) {
            Object actual = iterator.actual();
            if (actual!=null) llamadas.add((Llamada) actual);
            iterator.siguiente();
        }
        return llamadas;
    }

    public ArrayList<String[]> getDatosRespuestas(){
        ArrayList<String[]> datosRespuestas = new ArrayList<>();
        for (RespuestaDeCliente respuestaDeCliente: this.seleccionada.getRespuestasDeEncuesta()){
            datosRespuestas.add(
                    new String[]{
                    		respuestaDeCliente.getDescripcionPregunta(todasLasEncuestas),
                            respuestaDeCliente.getDescripcionRespuesta(),
                            respuestaDeCliente.getDescripcionEncuesta(todasLasEncuestas)
                    }
            );
        }
        return datosRespuestas;
    }

    public void tomarOpcionCsv(PantallaConsultarEncuestas pantallaConsultarEncuestas, String nombreCliente, String estadoActual, int duracion, String[][] datosRespuestas){
        pantallaConsultarEncuestas.notificarGeneracionArchivo(generarCsv(nombreCliente, estadoActual, duracion, datosRespuestas));
        finDelCu();
    }

    public String generarCsv(String nombreCliente, String estadoActual, int duracion, String[][] datosRespuestas){
        Workbook libro = new XSSFWorkbook();
        org.apache.poi.ss.usermodel.Sheet hoja = libro.createSheet();

        // cabecera
        Row fila = hoja.createRow(1);
        Cell celda = fila.createCell(1);
        celda.setCellValue("Nombre: ");
        celda = fila.createCell(2);
        celda.setCellValue(nombreCliente);
        celda = fila.createCell(4);
        celda.setCellValue("Estado actual: ");
        celda = fila.createCell(5);
        celda.setCellValue(estadoActual);
        celda = fila.createCell(7);
        celda.setCellValue("Duracion");
        celda = fila.createCell(8);
        celda.setCellValue(String.valueOf(duracion));
          


        fila = hoja.createRow(3);
        celda = fila.createCell(1);
        celda.setCellValue("ENCUESTA: ");
        celda = fila.createCell(2);
        celda.setCellValue(datosRespuestas[0][2]);
        
        int cont = 0;
        
        for (int i=0; i<datosRespuestas.length; i++) {
        	 fila = hoja.createRow(cont+5+i);
        	 celda = fila.createCell(2);
             celda.setCellValue("Descripcion Pregunta");
             
             celda = fila.createCell(3);
             celda.setCellValue(datosRespuestas[i][1]);
             
             
             fila = hoja.createRow(cont+5+i+1);
             celda = fila.createCell(2);
             celda.setCellValue("Respuesta:");
             
             celda = fila.createCell(3);
             celda.setCellValue(datosRespuestas[i][0]);
             cont+=1;
        }
        
        //Exporto el archivo
        try {
            String nameInforme = "InformeEncuestasLlamadas.xlsx";
            System.out.println(nameInforme);
            OutputStream exportar = new FileOutputStream(nameInforme);
            libro.write(exportar);
            exportar.close();
            return nameInforme;
        } catch(Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public void finDelCu(){

    }

    @Override
    public Iterator crearIterator(Object[] elementos) {
        ArrayList<Predicate<Llamada>> filtros = new ArrayList<>();
        filtros.add(elem -> elem.esDePeriodo(this.fechaInicio, this.fechaFin));
        filtros.add(Llamada::tieneRespuesta);
        return new IteradorLlamadas(elementos, filtros);
    }
}


